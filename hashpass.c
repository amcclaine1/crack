#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

int main(int argc, char *argv[])
{
    FILE *src = fopen(argv[1], "r");
    if(!src) 
    { 
        printf("Could not find file for reading."); 
        exit(1);
    }
    
    FILE *dest = fopen(argv[2], "w");
    
    char temp[1000];
    while(fgets(temp, 1000, src) != NULL)
    {
        int length = strlen(temp) - 1;
        char hashed[1000];
        strcpy(hashed, md5(temp, length));
        hashed[strlen(hashed)] = '\n';
        fprintf(dest, "%s", hashed);
    }
}